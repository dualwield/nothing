package main

const HTMLTemplate = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Report</title>
</head>
<body>
	<h2> {REPORT-NAME} at {TIME} </h2>
	<div id="item1"> {ITEM1} </div>
	<div id="item2"> {ITEM2} </div>
</body>
</html>
`
