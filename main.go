package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"
)

func main() {
	fmt.Println("creating a NOTHING report")
	now := time.Now().UTC()

	reportName := func() (name string) {
		name = os.Getenv("REPORT_NAME")
		if name == "" {
			name = "report"
		}
		return
	}()

	schema := struct {
		Name      string `json:"name"`
		OtherInfo string `json:"other"`
		Timestamp int64  `json:"ts"`
	}{
		Name:      "Gitlab Pipelines Learning Report",
		OtherInfo: "This is a project where I am learning how to use these Gitlab pipelines",
		Timestamp: now.Unix(),
	}

	b, err := json.Marshal(schema)
	if err != nil {
		log.Fatalln(err) // NOTE generally I hate doing this, but for learning let it be done
	}

	if err := ioutil.WriteFile(fmt.Sprintf("%s.json", reportName), b, 666); err != nil {
		log.Fatalln(err)
	}

	fmt.Println(fmt.Sprintf("done with report.. [%s.json]", reportName))

	fmt.Println("Building HTML Report")
	html := strings.ReplaceAll(HTMLTemplate, "{REPORT-NAME}", reportName)
	html = strings.ReplaceAll(html, "{TIME}", now.Format(time.ANSIC))
	html = strings.ReplaceAll(html, "{ITEM1}", schema.Name)
	html = strings.ReplaceAll(html, "{ITEM2}", schema.OtherInfo)

	if err := ioutil.WriteFile(fmt.Sprintf("%s.html", reportName), []byte(html), 0755); err != nil {
		log.Fatalln(err)
	}
}
